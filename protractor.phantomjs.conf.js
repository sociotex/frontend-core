/**
 * Prepare:
 * `sudo -H npm i -g protractor phantomjs`
 *
 * Start:
 * `phantomjs --webdriver=9515`
 */
exports.config = require('./protractor.common.js')({
    seleniumAddress: 'http://localhost:9515',
    capabilities: {
        'browserName': 'phantomjs'
    },
    'phantomjs.binary.path': './node_modules/phantomjs/bin/phantomjs',
});
