[Главная](index.md)

[Подготовка окружения]()

  * [Технологии](pages/technologies.md)
  * [Установка ПО](pages/software.md)
  * [Запуск локального сервера](pages/localServer.md)
  - - - -
  * # Настройка IDE
  * [Настройка VS](pages/settingUpVS.md)
  * [Настройка WebStorm](pages/settingUpWS.md)
  * [Настройка Sublime Text](pages/settingUpST.md)

[Разработка]()

  * [Структура проекта](pages/structure.md)
  * [Создание нового модуля](pages/newModule.md)
  * [Работа с зависимостями](pages/dependencies.md)
  * [Работа с API](pages/api.md)
  * [Локальная сборка](pages/localBuild.md)
  - - - -
  * [Написание кода на TypeScript](pages/typescript.md)
  * [Стили CSS](pages/styles.md)
  - - - -
  * [Модульное тестирование](pages/unitTests.md)
  * [Интеграционное тестирование](pages/e2eTests.md)
  - - - -
  * [Документирование кода](pages/docComments.md)
  * [Прочая документация](pages/docs.md)

[Деплой](pages/deploy.md)

[Прочее]()

 * [Задачи сборщика Gulp](pages/gulp.md)
 * [Правила наименования коммитов](pages/commits.md)
 * [Стиль кода](pages/codeStyles.md)