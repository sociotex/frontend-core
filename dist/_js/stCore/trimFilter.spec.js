/// <reference path="../all.d.ts"/>
define(["require", "exports", 'stCore/module', 'lodash', 'ngMockE2E'], function (require, exports, module_1) {
    describe(module_1["default"] + '.trimFilter', function () {
        beforeEach(angular.mock.module(module_1["default"]));
        var trim;
        beforeEach(inject(function ($filter) {
            trim = $filter('trim');
        }));
        it('resolved', function () {
            expect(_.isFunction(trim)).toBe(true);
        });
        it('default 50', function () {
            var result = trim('very long text very long text very long text very long text very long text very long text ');
            expect(result).toBe('very long text very long text very long text ve...');
            expect(result.length).toBe(50);
        });
        it('set max length', function () {
            var result = trim('very long text very long text very long text very' +
                ' long text very long text very long text', 10);
            expect(result).toBe('very lo...');
            expect(result.length).toBe(10);
        });
    });
});
