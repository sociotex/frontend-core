﻿var tests = [];
for (var file in window.__karma__.files) {
    if (/.spec\.js$/.test(file)) {
        tests.push(file);
    }
}

define('angular', [], angular);

function getJson(url) {
    'use strict';

    var r = new XMLHttpRequest();
    r.open('GET', url, false);
    r.send();
    return JSON.parse(r.responseText);
}

var appConfig = getJson('/base/src/requireConfig.json');
appConfig.paths.ngMockE2E = '../_libs/angular-mocks/angular-mocks';
appConfig.shim.ngMockE2E = ['angular'];

require.config({
    // Karma serves files under /base, which is the basePath from your config file
    baseUrl: '/base/src/_js',

    paths: appConfig.paths,

    shim: appConfig.shim,

    // dynamically load all test files
    deps: tests,

    // we have to kickoff jasmine, as it is asynchronous
    callback: function () {
        'use strict';

        require(['ngMockE2E'], function () {
            angular.testing = true;
            window.__karma__.start();
        });
    }
});
