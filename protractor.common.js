module.exports = function (config) {
    "use strict";
    if ( !config.hasOwnProperty('onPrepare') ) {
        config.onPrepare = function () {
            var SpecReporter = require('jasmine-spec-reporter');
            // add jasmine spec reporter
            jasmine.getEnv().addReporter(new SpecReporter({
                displayStacktrace: false,     // display stacktrace for each failed assertion
                displayFailuresSummary: true, // display summary of all failures after execution
                displaySuccessfulSpec: true,  // display each successful spec
                displayFailedSpec: true,      // display each failed spec
                displayPendingSpec: false,    // display each pending spec
                displaySpecDuration: true,   // display each spec duration
                displaySuiteNumber: false,    // display each suite number (hierarchical)
                colors: {
                    success: 'green',
                    failure: 'red',
                    pending: 'grey'
                },
                prefixes: {
                    success: '✔ ',
                    failure: '✖ ',
                    pending: '- '
                },
                customProcessors: []
            }));
        };
    }

    if ( !config.hasOwnProperty('jasmineNodeOpts') ) {
        config.jasmineNodeOpts = {
            print: function () {
            }
        };
    }

    if ( !config.hasOwnProperty('framework') ) {
        config.framework = 'jasmine2';
    }

    if ( !config.hasOwnProperty('suites') ) {
        config.suites = require('./e2eSuites.js');
    }

    if ( !config.hasOwnProperty('baseUrl') ) {
        config.baseUrl = 'http://localhost:9500/';
    }

    return config;
};
