declare namespace angular {
    interface ILocaleDateTimeFormatDescriptor {
        FIRSTDAYOFWEEK: number;
    }

    interface IAngularStatic {
        testing: boolean;
    }

    interface IWindowService {
        _get(name: string): any;
        _logPromise(p: ng.IPromise<any>): void;
    }

    namespace cache {
        interface ICacheConfig {
            storageMode: string;
        }

        interface ICacheFactoryProvider {
            defaults?: ICacheConfig;
        }

        interface ICacheFactory {
            get(name: string): ng.ICacheObject;
            createCache(name: string, config?: ICacheConfig): ICacheObject;
        }
    }

    namespace permission {
        interface IPermissionService {
            defineRole(name: string,
                       checker: (params: ng.ui.IStateParamsService) =>
                           boolean|ng.IPromise<boolean>): IPermissionService;
            defineManyRoles(names: string[],
                            checker: (params: ng.ui.IStateParamsService, name: string) =>
                                boolean|ng.IPromise<boolean>): IPermissionService;
        }
    }
}

declare namespace msgpack {
    interface IMsgPack {
        Buffer: IBuffer;
        encode(obj: any): any;
        decode(msg: any): any;
        register(type: number, cls: Function, encoder: Function, decoder: Function): void;
    }

    interface IBuffer {
        new (args: any): IBuffer;
        writeUInt32BE(value: number): void;
        readUInt32BE(offset: number): number;
    }

    interface IMsgPackFactory {
        (): IMsgPack;
    }
}

/* tslint:disable */
declare module 'msgpack5' {
    let msgpack: msgpack.IMsgPackFactory;
    export = msgpack;
}
