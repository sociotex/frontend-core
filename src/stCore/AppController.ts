export default class AppController {

    /**
     * @ngInject
     */
    constructor($scope: any,
                $menu: St.IMenuCategoryConfig[],
                $state: ng.ui.IStateService, appConfig: any) {
        $scope.app = appConfig;
        $scope.menu = $menu;
        $scope.$state = $state;
    }
}
