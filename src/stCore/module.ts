/// <reference path="module.d.ts"/>

import AppController from './AppController';
import $menuProvider from './$menuProvider';
import trimFilter from './trimFilter';
import uiToggleClass from './uiDirectives/uiToggleClass';
import uiNav from './uiDirectives/uiNav';
import help from './directives/help';
import {Helpers} from './services/$helpers';
import {PromptsService} from './services/$prompts';
import {HelpService} from './services/$help';
import 'angular';
import 'angular-locale_ru';
import 'ui.bootstrap';
import 'angularScreenfull';
import 'toaster';
import 'ngAnimate';
import 'ngFileUpload';
import 'angular-cache';
import '../../_libs/kn-appcache/dist/kn-appcache';
// import msgpack = require('msgpack5');

let moduleName = 'stCore';
let dependencies = [
    'ngAnimate',
    'ui.bootstrap',
    'angular-cache',
    'angularScreenfull',
    'toaster',
    'ngFileUpload',
    'kn-appcache'
];

angular.module(moduleName, dependencies)
    // aliases
    .factory('$upload', (Upload: ng.angularFileUpload.IUploadService) => Upload)
    .factory('$messages', (toaster: ngtoaster.IToasterService) => toaster)
    .constant('appConfig', {
        name: 'Sociotex',
        version: '0.0.1',
        // for chart colors
        color: {
            primary: '#7266ba',
            info: '#23b7e5',
            success: '#27c24c',
            warning: '#fad733',
            danger: '#f05050',
            light: '#e8eff0',
            dark: '#3a3f51',
            black: '#1c2b36'
        },
        settings: {
            themeID: 1,
            navbarHeaderColor: 'bg-black',
            navbarCollapseColor: 'bg-white-only',
            asideColor: 'bg-black',
            headerFixed: true,
            asideFixed: false,
            asideFolded: false,
            asideDock: false,
            container: false
        }
    })
    .filter({
        trim: trimFilter
    })
    .provider({
        $menu: $menuProvider
    })
    .controller({
        AppController: AppController
    })
    .directive({
        uiToggleClass: uiToggleClass,
        uiNav: uiNav,
        help: help
    })
    .service({
        $helpers: Helpers,
        $help: HelpService,
        $prompts: PromptsService
    })
    .run(($appCache: KN.IAppCache) => {
        $appCache.init();
    })
    .run(($rootScope: St.IRootScope,
          $injector: ng.auto.IInjectorService,
          $window: ng.IWindowService) => {
        $rootScope._ = _;

        $window._get = <any>_.bind($injector.get, $injector);
        $window._logPromise = (p: ng.IPromise<any>) => {
            p.then(
                <any>_.bind(console.log, console),
                <any>_.bind(console.error, console)
            );
        };
    });

export default moduleName;
