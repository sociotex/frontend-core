import 'lodash';

export class PromptsService implements St.IPromptsService {
    /**
     * @ngInject
     */
    constructor(private $uibModal: ng.ui.bootstrap.IModalService,
                private $rootScope: St.IRootScope) {
    }

    public promptYesNo(message: string, title?: string) {
        let scope = this.$rootScope.$new();
        _.assign(scope, {
            title: title,
            message: message
        });
        return this.$uibModal.open({
            templateUrl: 'stCore/parts/promptYesNo.html',
            scope: scope
        }).result;
    }

    public promptText(message: string, title?: string, defaultText = '') {
        let scope = this.$rootScope.$new();
        _.assign(scope, {
            title: title,
            message: message,
            text: defaultText
        });
        return this.$uibModal.open({
            templateUrl: 'stCore/parts/promptText.html',
            scope: scope
        }).result;
    }

    public prompt() {
        console.warn('DEPRICATED $prompt.prompt() method');
        return _.bind(this.promptYesNo, arguments);
    }
}
