import 'lodash';

export class Helpers implements St.IHelpers {
    /**
     * @ngInject
     * @param $q
     * @param $rootScope
     */
    constructor(private $q: ng.IQService,
                private $rootScope: ng.IRootScopeService) {
    }

    public wrapDigest(cb: Function) {
        return _.wrap(cb, (f: Function, ...args: any[]) => {
            f.apply(undefined, args);
            this.$rootScope.$digest();
        });
    }

    public readFileContent(file: File) {
        let deferred = this.$q.defer();

        let reader = new FileReader();
        reader.onload = () => deferred.resolve(reader.result);
        reader.onerror = (e: Event) => deferred.reject(e);
        reader.readAsText(file);
        return deferred.promise;
    }

    public formatIdList(ids: string) {
        return _(ids.split('\n'))
            .map(s => s.trim())
            .compact()
            .uniq()
            .sort()
            .value()
            .join('\n');
    }
}
