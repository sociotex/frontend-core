/// <reference path="../../all.d.ts"/>

import 'lodashExt';
import 'ngMockE2E';
import stCore from 'stCore/module';

describe(stCore + '.$prompts service', () => {

    beforeEach(angular.mock.module(stCore));

    it('should open modal when prompt', inject(($prompts: St.IPromptsService,
                                                $uibModal: ng.ui.bootstrap.IModalService) => {

        spyOn($uibModal, 'open').and.callThrough();

        let res = $prompts.promptYesNo('Сообщение', 'Опциональный заголовок');

        expect(_.isPromise(res)).toBe(true);

        let config = (<jasmine.Spy><any>$uibModal.open).calls.first().args[0];
        expect(config.scope.message).toBe('Сообщение');
        expect(config.scope.title).toBe('Опциональный заголовок');
        expect(config.templateUrl).toBe('stCore/parts/promptYesNo.html');
    }));
});

