import 'lodash';

interface IHelpModalScope extends ng.IScope {
    title: string;
    body: string;
}

export class HelpService implements St.IHelpService {
    private _cache: ng.ICacheObject;

    /**
     * @ngInject
     */
    constructor(private $uibModal: ng.ui.bootstrap.IModalService,
                private $q: ng.IQService,
                private $rootScope: ng.IRootScopeService,
                private $sce: ng.ISCEService,
                private $timeout: ng.ITimeoutService,
                $cacheFactory: ng.ICacheFactoryService) {
        this._cache = $cacheFactory('help');
    }

    public open(helpId: string) {
        return this._loadArticle(helpId)
            .then(help => {
                let scope = <IHelpModalScope>this.$rootScope.$new();
                scope.title = help.title;
                scope.body = help.body;
                return this.$uibModal.open({
                    templateUrl: 'stCore/parts/helpModal.html',
                    scope: scope
                }).result;
            });
    }

    public loadHint(hintId: string) {
        let cached = <St.IHelpHint>this._cache.get(hintId);
        if ( cached ) {
            return this.$q.when(cached);
        }

        // TODO load from server
        let hint = <St.IHelpHint>{
            id: hintId,
            body: 'test hint'
        };
        return this.$timeout(2000)
            .then(() => hint);
    }

    private _loadArticle(helpId: string): ng.IPromise<any> {
        let cached = <St.IHelpArticle>this._cache.get(helpId);
        if ( cached ) {
            return this.$q.when(cached);
        }

        // TODO load from server
        return this.$q.when(<St.IHelpArticle>{
            id: helpId,
            title: 'test.help.title',
            body: this.$sce.trustAsHtml('<h3>test</h3><p>test <b>test</b></p>')
        });
    }
}
