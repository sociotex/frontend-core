import 'lodash';

let trimFilter = () => {
    return (text: string, maxLength = 50) => {
        return _.truncate(text, maxLength);
    };
};
export default trimFilter;
