import 'angular';
import 'jquery';
import 'lodash';

interface IUiToggleClassAttributes extends ng.IAttributes {
    uiToggleClass: string;
    target: string;
}

export default _.constant({
    restrict: 'AC',
    link: (scope: ng.IScope, el: JQuery, attr: IUiToggleClassAttributes) => {
        el.on('click', e => {
            e.preventDefault();
            let classes = attr.uiToggleClass.split(','),
                targets = (attr.target && attr.target.split(',')) || [el],
                key = 0;

            angular.forEach(classes, className => {
                let target = targets[(targets.length && key)];
                if ( className.indexOf('*') !== -1 ) {
                    magic(className, target);
                }
                $(target).toggleClass(className);
                key++;
            });

            $(el).toggleClass('active');

            function magic(className: string, target: string|JQuery) {
                let patt = new RegExp('\\s' +
                    className
                        .replace(/\*/g, '[A-Za-z0-9-_]+')
                        .split(' ')
                        .join('\\s|\\s') + '\\s', 'g');
                let cn = ` ${$(target)[0].className} `;
                while (patt.test(cn)) {
                    cn = cn.replace(patt, ' ');
                }
                $(target)[0].className = $.trim(cn);
            }
        });
    }
})
