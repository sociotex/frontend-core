import 'lodash';

const enum HelpMode {
    Hint,
    Article
}

class HelpController {

    public hint: St.IHelpHint;
    public hintId: string;
    public helpId: string;

    /**
     * @ngInject
     * @param $help
     */
    constructor(private $help: St.IHelpService) {

    }

    public openArticle() {
        if ( this.helpId ) {
            this.$help.open(this.helpId);
        }
    }

    public loadHint() {
        if ( this.hintId && !this.hint ) {
            this.$help.loadHint(this.hintId)
                .then(hint => {
                    this.hint = hint;
                });
        }
    }

    public getTooltip() {
        if ( this.hintId ) {
            return this.hint ? this.hint.body : 'Загрузка...';
        } else if ( this.helpId ) {
            return 'Открыть справку';
        }
    }

}

let factory = _.constant(<ng.IDirective>{
    templateUrl: 'stCore/directives/help.html',
    controller: HelpController,
    controllerAs: 'help',
    bindToController: true,
    scope: { hintId: '@', helpId: '@' }
});

export default factory;
