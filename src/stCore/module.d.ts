/// <reference path="../all.d.ts"/>

declare namespace St {

    interface IRootScope extends ng.IRootScopeService {
        _: _.LoDashStatic;
        showBackdrop: boolean;
    }

    interface IMenuCategoryConfig {
        title: string;
        weight: number;
        items?: IMenuItemConfig[];
    }

    interface IMenuItemConfig {
        title: string;
        state?: string;
        parentState?: string;
        stateParams?: Object;
        url?: string;
        items?: IMenuItemConfig[];
        icon?: string;
        color?: string;
        weight?: number;
        category?: IMenuCategoryConfig;
        children? : IMenuItemConfig[];
        needRoles?: string[];
    }

    interface IMenuProvider {
        add(item: IMenuItemConfig): IMenuProvider;
        $get(): IMenuCategoryConfig[];
    }

    interface IListItem {
        id: number|string;
        name: string;
    }

    interface IHelpers {
        wrapDigest(cb: Function): Function;
        readFileContent(file: File) : ng.IPromise<string>;
        formatIdList(ids: string): string;
    }

    interface IHelpHint {
        id: string;
        body: string;
    }

    interface IHelpArticle extends IHelpHint {
        title: string;
    }

    interface IHelpService {
        open(helpId: string): ng.IPromise<any>;
        loadHint(hintId: string): ng.IPromise<IHelpHint>;
    }

    interface IPromptsService {
        promptYesNo(message: string, title?: string): ng.IPromise<void>;
        promptText(message: string, title?: string, defaultText?: string): ng.IPromise<string>;
    }
}
