import 'lodash';

let categories: St.IMenuCategoryConfig[] = [];
let weightSortFn = (a: { weight?: number }, b: { weight?: number }) => (a.weight || 0) - (b.weight || 0);

export default _.constant(<St.IMenuProvider>{
    add: (item) => {

        let category = _.find(categories, { title: item.category.title });
        if ( category ) {
            item.category = category;
            category.items.push(item);
        } else {
            category = item.category;
            category.items = [item];
            categories.push(item.category);
            categories.sort(weightSortFn);
        }
        category.items.sort(weightSortFn);

        return this; // allow chaining
    },
    $get: () => categories
});
