/// <reference path="./all.d.ts" />

import 'angular';
import 'ui.router';
import 'msgpack5';
import './_libs/kn-appcache/dist/kn-appcache';

import stCore from 'stCore/module';
import templates = require('templates'); // все шаблоны проекта

let moduleName = 'stDemo';
let dependencies = [
    'ui.router',
    'templates',
    'kn-appcache',
    stCore
];

angular.noop(templates); // keep templates imported

angular.module(moduleName, dependencies)
    .config(($locationProvider: ng.ILocationProvider, $stateProvider: ng.ui.IStateProvider) => {

        $locationProvider.html5Mode(true);

        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: '_design/parts/_layout.html'
            });
    });

export default moduleName;
