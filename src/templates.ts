﻿import 'angular';

let moduleName = 'templates';

angular.module(moduleName, []); // replaced in build-time with gulp

export = moduleName;
