(function () {
    'use strict';

    function getJson (url, cb) {
        var r = new XMLHttpRequest();
        r.open('GET', url, true);
        r.onreadystatechange = function () {
            if ( r.readyState === 4 && r.status === 200 ) {
                cb(JSON.parse(r.responseText));
            }
        };
        r.send();
    }

    getJson('./requireConfig.json', function (config) {
        require.config(config);

        require(['./app'], function (appModule) {
            angular.bootstrap(document.documentElement, [appModule.default]);
        });
    });
}());
