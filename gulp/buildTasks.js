﻿'use strict';
var gulp = require('gulp');

module.exports = {
    compile: function (config, cb) {
        var requirejs = require('requirejs');

        requirejs.optimize(config, function (result) {
            console.log(result);
            cb();
        }, function (err) {
            cb(err);
        });
    },

    htmlmin: function (src, options) {
        var htmlmin = require('gulp-htmlmin');

        return gulp.src(src, options)
            .pipe(htmlmin({
                removeComments: true,
                collapseWhitespace: true,
                conservativeCollapse: true,
                minifyJS: true
            }));
    },

    compress: function (src, options) {
        var uglify = require('gulp-uglify');

        return gulp.src(src, options)
            .pipe(uglify({
                output: {ascii_only: true}
            }));
    },

    buildNgTemplates: function (src, moduleName) {
        var templateCache = require('gulp-angular-templatecache');

        return gulp.src(src)
            .pipe(templateCache({
                module: moduleName,
                standalone: true,
                moduleSystem: 'RequireJs'
            }));
    },

    styles: function (src, options) {
        var sass = require('gulp-sass');
        var notify = require('gulp-notify');
        var postcss = require('gulp-postcss');
        var autoprefixer = require('autoprefixer');

        var processors = [
            autoprefixer({browsers: ['last 2 version', 'ie >= 9']})
        ];

        return gulp.src(src, options)
            .pipe(sass().on('error', sass.logError))
            .pipe(notify({
                message: 'Compiled: <%= file.relative %>'
            }))
            .on('error', function (err) {
                notify.onError({
                    message: 'Error: <%= error.message %>',
                    title: 'Compile error'
                })(err);
                this.emit('end');
            })
            .pipe(postcss(processors));
    }
};
