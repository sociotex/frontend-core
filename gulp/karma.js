﻿'use strict';

var path = require('path');
var KarmaServer = require('karma').Server;

function start(config, cb) {
    var server = new KarmaServer(config, cb);
    server.start();
}

module.exports = {
    test: function (cb, config) {
        start({
            configFile: path.resolve(config),
            singleRun: true
        }, function () {
            cb();
        });
    },

    tdd: function (cb, config) {
        start({
            configFile: path.resolve(config),
            reporters: ['mocha']
        }, cb);
    },

    reports: function (cb, config) {
        start({
            configFile: path.resolve(config),
            singleRun: true,
            reporters: ['junit', 'coverage']
        }, function () {
            cb();
        });
    }
};
