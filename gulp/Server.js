'use strict';

var http = require('http');
var fs = require('fs');
var URL = require('url');

function Server () {
    var extensions = Object.keys(this.mimeTypes).join('|');
    this._clientStaticRegExp = new RegExp('\\.(' + extensions + ')$');
}

Server.prototype = {
    workDir: 'src',

    mimeTypes: {
        html: 'text/html',
        js: 'application/javascript',
        json: 'application/json',
        css: 'text/css',
        gif: 'image/gif',
        png: 'image/png',
        jpg: 'image/jpeg',
        woff: 'application/font-woff',
        woff2: 'application/font-woff',
        svg: 'image/svg+xml',
        appcache: 'text/cache-manifest',
        pdf: 'application/pdf'
    },

    getMimeType: function (filePath) {
        var matches = /\.(\w+)$/.exec(filePath);
        var ext = matches[1];
        return this.mimeTypes[ext] || 'application/octet-stream';
    },

    sendFile: function (res, path) {
        var self = this;
        fs.exists(path, function (exists) {
            if ( !exists ) {
                console.error(path);
                self.sendError(res, 404);
                return;
            }

            fs.readFile(path, function (err, contents) {

                if ( err ) {
                    console.dir(err);
                    return;
                }

                res.writeHead(200, {
                    'Content-Type': self.getMimeType(path)
                });
                res.end(contents);

            });
        });
    },

    sendError: function (res, code) {
        res.writeHead(code, {
            'Content-Type': 'text/html'
        });
        res.end('<h1>Error ' + code + '</h1>');
    },

    isClientStatic: function (url) {
        return this._clientStaticRegExp.test(url);
    },

    requestHandler: function (req, res) {
        var url = URL.parse(req.url);
        console.log('[' + req.method + '] ', url.pathname);

        var path = this.isClientStatic(url.pathname) ?
            url.pathname :
            '/index.html';

        this.sendFile(res, this.workDir + path);
    },

    run: function (port) {
        console.log('Server started on ' + port + ' port');

        http
            .createServer(this.requestHandler.bind(this))
            .listen(port);
    },

    /**
     * @type RegExp
     */
    _clientStaticRegExp: null
};

module.exports = Server;
