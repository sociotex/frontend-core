﻿module.exports = function (config) {
    'use strict';

    config.set({
        basePath: '',
        frameworks: ['jasmine', 'requirejs'],

        files: [
            // first angular for templates
            { pattern: 'src/_libs/jquery/dist/jquery.js', includes: true },
            { pattern: 'src/_libs/angular/angular.js', includes: true },

            // all application files
            { pattern: 'src/**/*.+(js|html|tpl|json)', included: false },

            // angular templates
            { pattern: 'src/!(_libs)/**/*.html', included: true },

            // loader
            { pattern: 'karma-loader.js' }
        ],

        exclude: [
            'src/**/*.e2e.js',
            'src/_libs/**/*Spec.js',
            'src/_libs/**/*.spec.js'
        ],

        preprocessors: {
            'src/_js/**/!(*spec).js': 'coverage',
            'src/**/*.html': 'ng-html2js'
        },

        ngHtml2JsPreprocessor: {
            stripPrefix: 'src/',
            moduleName: 'templates'
        },

        reporters: ['mocha'],

        coverageReporter: {
            reporters: [
                {
                    type: 'html',
                    dir: 'reports/coverage/'
                },
                {
                    type: 'text-summary'
                },
                {
                    type: 'cobertura',
                    dir: 'reports',
                    subdir: '.',
                    file: 'covarage.xml'
                }
            ]
        },

        junitReporter: {
            outputFile: 'reports/unittests.xml'
        },

        mochaReporter: {
            ignoreSkipped: true
        },

        port: 9876,
        colors: true,
        logLevel: config.LOG_WARN,
        autoWatch: true,
        singleRun: false,

        //browsers: ['Chrome', 'Firefox'],
        //browsers: ['Chrome'],
        browsers: ['PhantomJS']
    });
};
