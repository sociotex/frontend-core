var argv = require('yargs').argv;
var bower = require('gulp-bower');
var buildTasks = require('./gulp/buildTasks');
var concat = require('gulp-concat');
var del = require('del');
var footer = require('gulp-footer');
var fs = require('fs');
var gulp = require('gulp');
var karma = require('./gulp/karma');
var ngAnnotate = require('gulp-ng-annotate');
var red = require('remove-empty-directories');
var replace = require('gulp-replace');
var ts = require('gulp-typescript');
var bump = require('gulp-bump');
var typedoc = require("gulp-typedoc");
var tslint = require('gulp-tslint');
var run = require('run-sequence');
var notify = require('gulp-notify');

function getJson(file) {
    var path = require('path').resolve(process.cwd(), file);
    var jsonFile = require('fs').readFileSync(path, 'utf8');
    return JSON.parse(jsonFile);
}

var requireConfig = getJson('./src/requireConfig.json');
var packageJson = getJson('./package.json');

gulp.task('_cleanCore', function () {
    return del(['src/stCore']);
});

gulp.task('installCore', ['_cleanCore'], function () {
    var coreFiles = [
        'src/_libs/sociotex-core/src/stCore/**',
        '!src/**/*.spec.ts',
        '!src/**/*.e2e.ts'
    ];
    return gulp.src(coreFiles, { base: 'src/_libs/sociotex-core/src' })
               .pipe(gulp.dest('src'))
});

//region Build

gulp.task('bower', function () {
    return bower({ cmd: 'install' })
    .pipe(gulp.dest('src/_libs'));
});

gulp.task('_cleanJs', function () {
    return del(['src/_js']);
});

gulp.task('ts', function () {
    var tsResult = gulp.src(['src/**/*.ts', '!src/_libs/**/*.ts'])
                       .pipe(ts({
                           noImplicitAny: true,
                           module: 'amd',
                           outDir: 'js',
                           typescript: require('typescript')
                       }));

    return tsResult.js
                   .pipe(ngAnnotate({
                       single_quotes: true
                   }))
                   .pipe(gulp.dest('src/_js'));
});

gulp.task('styles', function () {
    return buildTasks.styles('src/_design/index.scss')
                     .pipe(gulp.dest('src/_design'));
});

gulp.task('compile', function (cb) {
    var config = {
        appDir: 'src',
        dir: 'dist',
        modules: [
            {
                name: 'app',
                excludeShallow: [
                    'templates' // this file generates by gulp
                ]
            }
        ],
        optimize: 'none',
        fileExclusionRegExp: /\.less|\.gitkeep|\.ts|\.scss|\.spec\.js/,
        preserveLicenseComments: false,
        optimizeCss: 'standard',
        removeCombined: true,
        baseUrl: requireConfig.baseUrl,
        paths: requireConfig.paths,
        shim: requireConfig.shim
    };

    buildTasks.compile(config, cb);
});

gulp.task('uglify', function () {
    return buildTasks.compress([
                         'dist/_js/app.js',
                         'dist/_libs/requirejs/require.js'
                     ], { base: 'dist/_js' })
                     .pipe(gulp.dest('dist/_js'));
});

gulp.task('htmlmin', function () {
    return buildTasks.htmlmin([
                         'dist/**/*.html',
                         '!dist/_libs/**/*.html'
                     ])
                     .pipe(gulp.dest('dist'));
});

gulp.task('templates', function () {
    return buildTasks.buildNgTemplates([
                         'dist/**/*.html',
                         '!dist/*.html',
                         '!dist/_libs/**/*.html'
                     ], 'templates')
                     .pipe(replace('define(', 'define("templates", '))
                     .pipe(gulp.dest('dist/_js'));
});

gulp.task('combine', function () {
    if ( argv.apiUrl ) {
        requireConfig.apiUrl = argv.apiUrl;
    }
    requireConfig.urlArgs = 'v=' + packageJson.version;
    var configJson = JSON.stringify(requireConfig);

    var loaderWithMocks = `        
// LOADER
var requireConfig = ${configJson};
require.config(requireConfig);
require(['app', '_apiMocks/module'], function (appModule, mocks) { 
   var moduleName = appModule.default;
   angular.module(moduleName)
       .constant("apiUrl", localStorage.apiUrl || requireConfig.apiUrl);
   angular.bootstrap(document.documentElement, [appModule.default, mocks]);
});`;

    var loader = `
// LOADER
var requireConfig = ${configJson};
require.config(requireConfig);
require(['app'], function (appModule) { 
    var moduleName = appModule.default;
    angular.module(moduleName)
        .constant("apiUrl", localStorage.apiUrl || requireConfig.apiUrl);
    angular.bootstrap(document.documentElement, [appModule.default]);
});`

    return gulp.src([
                   'dist/_libs/requirejs/require.js',
                   'dist/_js/app.js',
                   'dist/_js/templates.js'
               ])
               .pipe(concat('app.js'))
               .pipe(footer(argv.mocks ? loaderWithMocks : loader))
               .pipe(gulp.dest('dist/_js'));
});

gulp.task('replaceScript', function () {
    return gulp.src('dist/index.html')
               .pipe(replace(
                   '<script src="_libs/requirejs/require.js" data-main="loader.js"></script>',
                   `<script src="_js/app.js?v=${packageJson.version}"></script>`))
               .pipe(replace(/\{version\}/g, packageJson.version))
               .pipe(gulp.dest('dist'));
});

gulp.task('clean', function () {
    return del([
        'dist/_libs/**/*.*',
        'dist/_libs/**/.*',
        'dist/_libs/**/LICENSE',
        'dist/_libs/**/modernizr', // WTH?
        '!dist/_libs/font-awesome/fonts/*.*',
        '!dist/_libs/sociotex-core/src/_design/theme/fonts/**/*.*',
        '!dist/_libs/angular-mocks/angular-mocks.js',

        'dist/**/*.html',
        '!dist/*.html',

        'dist/_js/templates.js',
        'dist/loader.js',
        'dist/requireConfig.json',
        'dist/build.txt'
    ])
    .then(function () {
        red('dist');
    });
});

gulp.task('addAppCache', function () {
    return gulp.src('dist/index.html')
               .pipe(replace(/<html (.+?)>/, '<html $1 manifest="/manifest.appcache">'))
               .pipe(replace(/\{version\}/g, packageJson.version))
               .pipe(gulp.dest('dist'));
});

gulp.task('updateCacheManifest', function () {
    return gulp.src('dist/manifest.appcache')
               .pipe(replace(/\{time\}/g, new Date().toJSON()))
               .pipe(replace(/\{version\}/g, packageJson.version))
               .pipe(gulp.dest('dist'));
});

gulp.task('_reportBuildEnd', function () {
    return gulp.src('.').pipe(notify({ message: 'Project built' }));
});

gulp.task('build', function (cb) {
    run(
        'bower',
        'installCore',
        '_cleanJs',
        ['ts', 'styles'],

        'compile',

        'uglify',
        'htmlmin',
        'templates',
        'combine',
        'replaceScript',
        ['addAppCache', 'updateCacheManifest'],
        'clean',
        '_reportBuildEnd',
        cb
    );
});

//endregion

// region Karma tests

gulp.task('test', function (done) {
    karma.test(done, './karma.conf.js');
});

gulp.task('tdd', function (done) {
    karma.tdd(done, './karma.conf.js');
});

gulp.task('test:reports', function (done) {
    karma.reports(done, './karma.conf.js');
});

// endregion

gulp.task('watch', ['ts:watch', 'styles:watch']);

gulp.task('styles:watch', ['styles'], function () {
    gulp.watch('src/**/*.scss', ['styles']);
});

gulp.task('ts:watch', ['ts'], function () {
    gulp.watch(['src/**/*.ts', '!src/_libs/**/*.ts'], ['ts']);
});

gulp.task('ts:lint', function () {
    return gulp.src(['src/**/*.ts', '!src/_libs/**/*.ts'])
               .pipe(tslint())
               .pipe(tslint.report(function (output, file, options) {
                   output.forEach(function (out) {
                       console.log(file.path + ':' + (out.startPosition.line + 1) + ' ' + out.failure + ' (' + out.ruleName + ')');
                   });
               }));
});

gulp.task('ts:doc', function () {
    return gulp.src(["src/**/!(*spec).ts"])
               .pipe(typedoc({
                   module: "amd",
                   target: "es5",
                   out: "tsDocs/",
                   // includeDeclarations: true,
                   hideGenerator: true,
                   name: "Frontend docs"
               }));
});

gulp.task('bump', function () {
    return gulp.src(['package.json', 'bower.json'])
               .pipe(bump())
               .pipe(gulp.dest('.'));
});

gulp.task('server', function () {
    var Server = require('./gulp/Server');
    var app = new Server();
    app.run(8500);
});

gulp.task('default', ['bower', 'ts', 'styles', 'server']);

module.exports = gulp;
