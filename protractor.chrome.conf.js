/**
 * Prepare:
 * `sudo -H npm i -g protractor`
 * `sudo -H webdriver-manager update`
 *
 * Start:
 * `webdriver-manager start`
 */
exports.config = require('./protractor.common.js')({
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions' : {
            args: ['--window-size=1125,700']
        },
    }
});
